<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ablog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B;~50RJ%L)sO6E=-j1}[&MbwZf@9q~Xf$I mD6M|*`&nQ^;@?t&^<!f=2|YbGf~]');
define('SECURE_AUTH_KEY',  'dWK>x|xNhpFlM65-(!0U;|{I_om5i{.a2 $5*Z~4gl] +_}Lv{W qKW7_*liLtkO');
define('LOGGED_IN_KEY',    '%2TT@>sC!g~hKz`s8:6:fDdFhO>[V*%3rI92~Gu3Xq!@] 7z:GX0lkW;lOJArCkT');
define('NONCE_KEY',        'p-5wuw2AMO[p?(-}A~&=<twYci#NRy$G?=z,~GIP3lcrJuokLm:vy8 @b6Td.slz');
define('AUTH_SALT',        ' C,*[QLyWK4SAWJa%tEI<9$z!%o!VIAuv_5Gguh7J?Sd3:UY! Tusi*; K780ZHB');
define('SECURE_AUTH_SALT', 'v17>dZL7sue[s*;/??k4LH]{Bb;;X)XX0Y3qC+@d.u0r2Q]:9iyzs5Ej#{?_Yd1P');
define('LOGGED_IN_SALT',   '^;E)BI0)ubfbox#Am;&N~teE3bUAs`MAK/_mLMSJMh)BRWSCdb.C9ooyGHWz,PCo');
define('NONCE_SALT',       'zkFbM{G4Yu@`/aQvV4w3(09.loZ2jz%8oU`VA2q@<otJ`kNVLI=g]S8wOp]W0X`H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ab_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
