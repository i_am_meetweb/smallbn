<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */


function codeneric_ad_manager_help_page() {
	global $cc_uam_config;
	$admin_edit_url = admin_url( 'edit.php' );


	?>

	<style>
		a {
			color: coral;
		}



	</style>
	<div class="wrap">
		<h1 style="font-weight: 100; margin: 1em 0;">Ultimate Ads Manager</h1>
		<div class="postbox">
			<div class="inside">
				<h3>Usage guide</h3>
				<ol>

					<li>
						First, <a target="_blank"
					              href="<?php echo add_query_arg( 'post_type', $cc_uam_config['custom_post_slug'], $admin_edit_url ); ?>">create</a>
						one or more ads.
					</li>
					<li>
						You can (but don't need to) group your ads by creating <a target="_blank"
						    href="<?php echo add_query_arg( 'post_type', $cc_uam_config['custom_post_slug'] . '_group', $admin_edit_url ); ?>">ad
							groups</a>.
						This is useful if you want to display multiple ads at the same spot.
					</li>
					<li>
						Now everything is set up to display your ads. You have two options here:
						<ol>
							<li>
								<h4>Widgets</h4>

								<p>
									A very simple method is to add the Ultimate Ads Manager widget into your sidebar.
									To do so, <a target="_blank" href="<?php echo admin_url( 'widgets.php' ); ?>">visit
										the widget page</a> and find the <strong>Ultimate Ads Widget</strong> under <em>Available
										Widgets</em>.
									<br> You can drag and drop this widget into a specific widget area as you would with
									any other widget.
								</p>

							</li>
							<li>
								<h4>Shortcode</h4>

								<p>
									Absolute control over the position of your ad is available via shortcodes.<br>
									A shortcode is a WordPress-specific code that lets you display plugin content with
									little effort.
									For the Ultimate Ads Manager plugin, the shortcodes look like this:
									<pre>[uam_ad id="1"]</pre>
									<a target="_blank" href="<?php echo add_query_arg( array('post_type' => $cc_uam_config['custom_post_slug']), $admin_edit_url ); ?>">Go to the ads overview</a> and you will see a column <em>shortcodes</em>,
									where the specific shortcode for every ad is listed.
									<a target="_blank"
									   href="<?php echo add_query_arg( 'post_type', $cc_uam_config['custom_post_slug'] . '_group', $admin_edit_url ); ?>">Ad
										groups</a> also have shortcodes.
									<br>
									All you have to do is to copy this code and paste it anywhere in your page via the <a
									target="_blank"
										href="<?php echo add_query_arg( array('post_type' => 'page'), $admin_edit_url ); ?>">Page editor</a>.
									The ad will then appear in the position you have defined.
								</p>

							</li>
						</ol>
					</li>

					<li>Go to <a target="_blank" href="<?php echo add_query_arg( array('post_type' => $cc_uam_config['custom_post_slug'], 'page' => 'statistics'), $admin_edit_url ); ?>">statistics</a> to see what happened on your site.</li>
				</ol>

				<strong>If you have problems or questions, please contact us using the <a href="<?php echo add_query_arg( array('post_type' => $cc_uam_config['custom_post_slug'], 'page' => 'codeneric_uam_support'), $admin_edit_url ); ?>">support page.</a></strong>
			</div>
		</div>
	</div>


	<?php
}