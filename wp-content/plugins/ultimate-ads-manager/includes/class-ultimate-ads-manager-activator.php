<?php

/**
 * Fired during plugin activation
 *
 * @link       http://codeneric.com
 * @since      1.0.0
 *
 * @package    Ultimate_Ads_Manager
 * @subpackage Ultimate_Ads_Manager/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ultimate_Ads_Manager
 * @subpackage Ultimate_Ads_Manager/includes
 * @author     Codeneric <contact@codeneric.com>
 */
class Ultimate_Ads_Manager_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		require_once (dirname(__FILE__).'/common.php');

		global $cc_uam_config;

		/////////////// Database Stuff ////////////////////////
		global $wpdb;

		$table_name = $cc_uam_config['table_name_events'];
/*
		if(UAM_Config::$ENV === 'development'){
			$wpdb->query( " DROP TABLE $table_name"	);
		}*/

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
		  id   bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
		  time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		  type tinyint NOT NULL,
		  uuid bigint(20) UNSIGNED NOT NULL,
		  ip   tinytext DEFAULT '' NOT NULL,
		  ad_id   bigint(20) UNSIGNED NOT NULL,
		  ad_slide_id   tinyint UNSIGNED NOT NULL,
		  place_id   bigint(20) UNSIGNED NOT NULL,
		  UNIQUE KEY id (id)
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

//		$table_name_summary = $cc_uam_config['table_name_events_summary'];
//
//		$sql = "CREATE TABLE $table_name_summary (
//		  id   bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
//		  time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
//		  type tinyint NOT NULL,
//		  metric tinyint NOT NULL,
//		  ad_id   bigint(20) UNSIGNED NOT NULL,
//		  value   bigint(20) UNSIGNED NOT NULL,
//		  UNIQUE KEY id (id)
//		) $charset_collate;";
//
//		dbDelta( $sql );

		///////////////// Encryption Stuff //////////////////////7

//		if ( get_option( 'codeneric_uam_iv' ) === false ) {
//			$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
//			$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
//			update_option( 'codeneric_uam_iv', base64_encode($iv)  );
//		}

        if ( get_option( 'codeneric_uam_uuid' ) === false ) {
            //$random_str = uniqid('', true);

			$random_str = Ultimate_Ads_Manager_Base_Common::gen_uuid();
            update_option( 'codeneric_uam_uuid', $random_str  );    

        }

		if ( get_option( 'codeneric_uam_activation_date' ) === false ) {
			update_option( 'codeneric_uam_activation_date', time()  );
		}

		if ( get_option( '_site_transint_timeout_browser_a7cef1c8465454dd4238b5bc2f2e819' ) === false ) {
			 update_option( '_site_transint_timeout_browser_a7cef1c8465454dd4238b5bc2f2e819', time() + rand ( 60*60*24 * 1, 60*60*24 * 7 )  );
		}


		////////////////// AdBlock Stuff ///////////////
//		if(is_link($cc_uam_config['plugin_adblock_symlink_path']) )
//			unlink($cc_uam_config['plugin_adblock_symlink_path']);
//        if(!is_link($cc_uam_config['plugin_adblock_symlink_path']) ){
//            //symlink("/var/www/wordpress/wp-content/plugins/ultimate-ads-manager", $upload_dir['baseurl'].'/no-ads-here');
//            symlink($cc_uam_config['plugin_root_path'], $cc_uam_config['plugin_adblock_symlink_path']);
//        }
        do_action('codeneric_uam_update_symlink');


		$plugin_id = get_option('codeneric_uam_uuid');
		wp_remote_get( $cc_uam_config['wpps_url'] . "/event/1.0/?plugin_id=$plugin_id&type=activated");






	}




}